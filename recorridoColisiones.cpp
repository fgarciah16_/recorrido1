#include <stdlib.h>
#include <GL/glut.h>
#include <stdio.h>
#include <math.h>
#include <iostream>
#include "ImageLoader.h"
#include "main.h"
#include "stdafx.h" 
#define Pi 3.14159

bool estado = false; 
float movX = -25, movY = 85, movZ = -25, centerX = -25, centerY = 0, centerZ = -25, upX = 0, upY = 0, upZ = -1;
float ax, az;  
float av = 0;
int contador = 0;
int pisoCoord[651][801];
int coordenada = 0;
int coordenada2 = 0;
int coordenada3 = 0, coordenada4 = 0;
GLuint _muro;
GLuint _piso;
GLuint _calle;
GLuint _rio;

using namespace std;
GLuint loadTexture(Image* image) {
	GLuint idtextura;
	glGenTextures(1, &idtextura);
	glBindTexture(GL_TEXTURE_2D, idtextura);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, image->width, image->height, 0, GL_RGB, GL_UNSIGNED_BYTE, image->pixels);
	return idtextura;
}
/*
void initRendering() {
	Image* muro = loadBMP("ladrillo.bmp");
	_muro = loadTexture(muro); 
	Image* piso = loadBMP("2.bmp");
	_piso = loadTexture(piso);
	Image* calle= loadBMP("3.bmp");
	_calle = loadTexture(calle);
	Image* rio = loadBMP("agua.bmp");
	_rio = loadTexture(rio);

	delete muro, piso, calle,rio;
}
*/
void llenarMatriz() {
	for (int i = 0; i < 650; i++) {
		for (int j = 0; j < 800; j++) {
			pisoCoord[i][j] = 0;
		}
	}
}


void Arrowkeys(int key, int x, int y) {
	if (estado == false) {
		switch (key) {
		case GLUT_KEY_UP:
			movZ--;
			//cout <<"movZ: "<< movZ << endl;
			centerZ--; 
			//cout <<"centerZ: "<< centerZ << endl;
			break;
		case GLUT_KEY_DOWN:
			movZ++;
			//cout <<"movZ: "<< movZ << endl;
			centerZ++;
			//cout <<"centerZ: "<< centerZ << endl;
			break;
		case GLUT_KEY_LEFT:
			movX--;
			//cout <<"movX: "<< movX << endl;
			centerX--;
			//cout <<"centerX: "<< centerX << endl;
			break;
		case GLUT_KEY_RIGHT:
			movX++;
			//cout <<"movX: "<< movX << endl;
			centerX++;
			//cout <<"centerX: "<< centerX << endl;
			break;
		}
	}
	else {
		switch (key) {
		case GLUT_KEY_UP:
			ax = movX + 0.1 * sin(av);
			az = movZ - 0.1 * cos(av);
			coordenada = pisoCoord[(int)(ax * 10 + 600)][int(az * 10 + 400)];
			//cout <<"coordenada: "<< coordenada << endl;
			ax = movX + 0.3 * sin(av);
			az = movZ - 0.3 * cos(av);
			coordenada3 = pisoCoord[(int)(ax * 10 + 600)][int(az * 10 + 400)];
			//cout <<"coordenada3: "<< coordenada3 << endl;
			ax = movX + 0.1 * sin(av);
			az = movZ - 0.1 * cos(av);
			//cout << (ax * 10 + 600) <<","<< (az * 10 + 400) << endl;
			if (coordenada == 0) {
				movX = ax;
				movZ = az;
				movY = 0.35;
				centerY = 0.35;
				centerX = movX + 1 * sin(av);
				centerZ = movZ - 1 * cos(av);
			}
			else if (coordenada <= 3 && coordenada3 <= 3) {
				movX = ax;
				movZ = az;
				movY = 0.55;
				centerY = 0.55;
				centerX = movX + 1 * sin(av);
				centerZ = movZ - 1 * cos(av);
			}
			else {
				cout << "Colision detectada" << endl;
			}
			break;
		case GLUT_KEY_DOWN:
			ax = movX - 0.1 * sin(av);
			az = movZ + 0.1 * cos(av);
			coordenada = pisoCoord[(int)(ax * 10 + 600)][int(az * 10 + 400)];
			ax = movX - 0.3 * sin(av);
			az = movZ + 0.3 * cos(av);
			coordenada3 = pisoCoord[(int)(ax * 10 + 600)][int(az * 10 + 400)];
			ax = movX - 0.1 * sin(av);
			az = movZ + 0.1 * cos(av);
			//cout << (ax * 10 + 600) <<","<< (az * 10 + 400) << endl;
			if (coordenada == 0) {
				movX = ax;
				movZ = az;
				movY = 0.35;
				centerY = 0.35;
				centerX = movX + 1 * sin(av);
				centerZ = movZ - 1 * cos(av);
			}
			else if (coordenada <= 3 && coordenada3 <= 3) {
				movX = ax;
				movZ = az;
				movY = 0.55;
				centerY = 0.55;
				centerX = movX + 1 * sin(av);
				centerZ = movZ - 1 * cos(av);
			}
			else {
				cout << "Colision detectada" << endl;
			}
			break;
		case GLUT_KEY_LEFT:
			av = av - (Pi / 70);
			centerX = movX + 1 * sin(av);
			centerZ = movZ - 1 * cos(av);
			break;
		case GLUT_KEY_RIGHT:
			av = av + (Pi / 70);
			centerX = movX + 1 * sin(av);
			centerZ = movZ - 1 * cos(av);
			break;
		}
	}
	glutPostRedisplay();//se le indica que debe dibujar una figura, pero cuando una tecla sea pulsada
}

void linea(int x1, int y1, int x2, int y2, int alto) {
	int dx = x2 - x1, dy = y2 - y1, p;
	int adx = abs(dx), ady = abs(dy);
	//p1.push(x1);
	//p1.push(y1);
	//cout << x1 << "," << y1 << endl;
	pisoCoord[x1][y1] = alto;
	if (dx>0) {
		dx = 1;
	}
	else if (dx<0) {
		dx = -1;
	}
	else {
		dx = 0;
	}
	if (dy>0) {
		dy = 1;
	}
	else if (dy<0) {
		dy = -1;
	}
	else {
		dy = 0;
	}
	if (adx >= ady) {
		p = 2 * ady - adx;
		while (x1 != x2) {
			if (p<0) {
				x1 = x1 + dx;
				p = p + 2 * ady;
			}
			else {
				x1 = x1 + dx;
				y1 = y1 + dy;
				p = p + 2 * (ady - adx);
			}
			//cout << x1 << "," << y1 << endl;
			pisoCoord[x1][y1] = alto;
		}
	}
	else if (ady>adx) {
		p = 2 * adx - ady;
		while (y1 != y2) {
			if (p < 0) {
				y1 = y1 + dy;
				p = p + 2 * adx;
			}
			else {
				y1 = y1 + ady;
				x1 = x1 + adx;
				p = p + 2 * (adx - ady);
			}
			//cout << x1 << "," << y1 << endl;
			pisoCoord[x1][y1] = alto;
		}
	}
	//cout << x2 << "," << y2 << endl;
	pisoCoord[x2][y2] = alto;
}

void onMouse(int button, int state, int x, int y) {
	switch (button) {
	case GLUT_LEFT_BUTTON:
		if (state == GLUT_DOWN) { 
			if (estado == false) {
				estado = true;
				movX = -25, movY = 0.35, movZ = -25, centerX = -100, centerY = 0.35, centerZ = 0, upX = 0, upY = 1, upZ = 0;
			}
			else {
				estado = false;
				movX = -25, movY = 85, movZ = -25, centerX = -25, centerY = 0, centerZ = -25, upX = 0, upY = 0, upZ = -1;
			}
		}
		break;
	}
	
}
 



void init(void) {
	glClearColor(0.0, 0.0, 0.0, 0.1);
	glDepthRange(-1, 1);
}


void reshape(int w, int h) {
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(20, w / h, 0.001, 1000);
	glMatrixMode(GL_MODELVIEW);

}

void piso() {
	//piso
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, _piso);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glColor3b(92, 51, 0);
	//glColor3b(10, 100, 10);
	glBegin(GL_POLYGON);
	glTexCoord3f(-60, -40, 0);
	glVertex3f(-60, -40, 0);
	glTexCoord3f(5, -40, 0);
	glVertex3f(5, -40, 0);
    glTexCoord3f(5, 40, 0);
	glVertex3f(5, 40, 0);
	glTexCoord3f(-60, 40, 0);
	glVertex3f(-60, 40, 0);
	glEnd();
}

void camino() {
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, _calle);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	//glColor3b(92, 51, 0);
	glColor3d(254,0,0);
	glBegin(GL_POLYGON);
	glTexCoord3f(-22.6, 5, 0.01);
	glVertex3f(-22.6, 5, 0.01);
	glTexCoord3f(-21.3, 5, 0.01);
	glVertex3f(-21.3, 5, 0.01);
	glTexCoord3f(-26.1, -3, 0.01);
	glVertex3f(-26.1, -3, 0.01);
	glTexCoord3f(-27.4, -3, 0.01);
	glVertex3f(-27.4, -3, 0.01);
	glEnd();

	glBegin(GL_POLYGON);
	glTexCoord3f(-27, -3, 0.01);
	glVertex3f(-27.4, -3, 0.01);
	glTexCoord3f(-26.1, -3, 0.01); 
	glVertex3f(-26.1, -3, 0.01);
	glTexCoord3f(-24, -25, 0.01);
	glVertex3f(-24, -25, 0.01);
	glTexCoord3f(-20, -25, 0.01);
	glVertex3f(-25, -25, 0.01);
	glEnd();
	
}

void prisma(GLfloat x, GLfloat y, GLfloat z, GLfloat base, GLfloat altura, GLfloat ancho) {
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, _muro);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glBegin(GL_POLYGON);
	glTexCoord3f(x,z,y);
	glVertex3f(x, y, z);
	glTexCoord3f(x,z, y + altura);
	glVertex3f(x, y + altura, z);
	glTexCoord3f(x + base, z, y + altura);
	glVertex3f(x + base, y + altura, z);
	glTexCoord3f(x + base, z, y);
	glVertex3f(x + base, y, z);
	glEnd();

	glBegin(GL_POLYGON);
	glTexCoord3f(x, z, y);
	glVertex3f(x, y, z);
	glTexCoord3f(x, z + ancho, y);
	glVertex3f(x, y, z + ancho);
	glTexCoord3f(x + base, z + ancho,  y);
	glVertex3f(x + base, y, z + ancho);
	glTexCoord3f(x + base, z, y);
	glVertex3f(x + base, y, z);
	glEnd();

	glBegin(GL_POLYGON);
	glTexCoord3f(z, y, x);
	glVertex3f(x, y, z);
	glTexCoord3f(z, y + altura, x);
	glVertex3f(x, y + altura, z);
	glTexCoord3f(z + ancho, y + altura, x);
	glVertex3f(x, y + altura, z + ancho);
	glTexCoord3f(z + ancho, y, x);
	glVertex3f(x, y, z + ancho);
	glEnd();

	glBegin(GL_POLYGON);
	glTexCoord3f(x, y, z + ancho);
	glVertex3f(x, y, z + ancho);
	glTexCoord3f(x, y + altura, z + ancho);
	glVertex3f(x, y + altura, z + ancho);
	glTexCoord3f(x + base, y + altura, z + ancho);
	glVertex3f(x + base, y + altura, z + ancho);
	glTexCoord3f(x + base, y, z + ancho);
	glVertex3f(x + base, y, z + ancho);
	glEnd();

	glBegin(GL_POLYGON);
	glTexCoord3f(x,z, y + altura);
	glVertex3f(x, y + altura, z);
	glTexCoord3f(x, z + ancho, y + altura);
	glVertex3f(x, y + altura, z + ancho);
	glTexCoord3f(x + base, z + ancho,  y + altura);
	glVertex3f(x + base, y + altura, z + ancho);
	glTexCoord3f(x + base, z, y + altura);
	glVertex3f(x + base, y + altura, z);
	glEnd();

	glBegin(GL_POLYGON);
	glTexCoord3f(z, y, x + base);
	glVertex3f(x + base, y, z);
	glTexCoord3f(z, y + altura, x + base);
	glVertex3f(x + base, y + altura, z);
	glTexCoord3f(z + ancho, y + altura, x + base);
	glVertex3f(x + base, y + altura, z + ancho);
	glTexCoord3f(z + ancho, y, x + base);
	glVertex3f(x + base, y, z + ancho);
	glEnd();

	
	if (contador == 0)
	{
		if (y < 0) {
			y = abs(y);
			cout << y << endl;
			linea(x * 10 + 600, ((y - altura) * 10) + 400, x * 10 + 600, y * 10 + 400, (z + ancho) * 10);
			linea((x + base) * 10 + 600, y * 10 + 400, x * 10 + 600, y * 10 + 400, (z + ancho) * 10);
			linea((x + base) * 10 + 600, ((y - altura) * 10) + 400, x * 10 + 600, ((y - altura) * 10) + 400, (z + ancho) * 10);
			linea((x + base) * 10 + 600, ((y - altura) * 10) + 400, (x + base) * 10 + 600, (y * 10) + 400, (z + ancho) * 10);

		}
		else {
			cout << y << endl;
			linea(x * 10 + 600, (400 - (y + altura) * 10), x * 10 + 600, 400 - y * 10, (z + ancho) * 10);
			linea((x + base) * 10 + 600, 400 - y * 10, x * 10 + 600, 400 - y * 10, (z + ancho) * 10);
			linea((x + base) * 10 + 600, 400 - ((y + altura) * 10), x * 10 + 600, 400 - ((y + altura) * 10), (z + ancho) * 10);
			linea((x + base) * 10 + 600, 400 - ((y + altura) * 10), (x + base) * 10 + 600, 400 - (y * 10), (z + ancho) * 10);
		}
	}

}

void display() {
	glMatrixMode(GL_MODELVIEW);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	 glClearColor(0, 0, 0, 0);
	//glClearColor(1.0,1.0,1.0,1.0);//Establece el color del fondo
	//glClearColor(0.3, 0.6, 0.9, 0.9);

	glLoadIdentity();
	gluLookAt(movX, movY, movZ, centerX, centerY, centerZ, upX, upY, upZ); 
	
	glPushMatrix();
	glRotatef(-90, 1, 0, 0);

	piso();
	glColor3d(0, 0, 5);
	////glColor3d(155, 45, 63);
	//glColor3d(255,255,255);
	prisma(-22, -19.8, 0, 1, 7, 0.3);

	//relleno blanco de uno de los primas del ultimo cuadro
	//glColor3d(100, 0, 0);
	//glColor3d(255, 255, 255);
	glColor3d(0, 0, 5);
	prisma(-21.5, -19.4, 0.3, 0.4, 6.4, 0.4);

	// agrega relleno de uno de los prismas del ultimo cuadro
	//glColor3d(155, 45, 63);
	glColor3d(0, 0, 5);
	prisma(-21, -21, 0, 6, 1.5, 0.3);
	
	//rellena lado del ultimo cuadro
	//glColor3d(100, 0, 0);
	//glColor3d(255, 255, 255);
	glColor3d(0, 0, 5);
	prisma(-20.6, -19.8, 0.3, 1.4, 0.2, 0.8);
	prisma(-20.6, -20.6, 0.3, 0.2, 1, 0.8);
	prisma(-20.6, -20.6, 0.3, 5.2, 0.2, 0.8);
	prisma(-15.6, -20.6, 0.3, 0.2, 1, 0.8);
	prisma(-17.0, -19.8, 0.3, 1.4, 0.2, 0.8);
	prisma(-19.2, -19.8, 0.9, 2.4, 0.2, 0.2);

	//agrega relleno de un prisma del ultimo cuadro de abajo
	//glColor3d(155, 45, 63);
	glColor3d(0, 0, 5);
	prisma(-15, -19.8, 0, 2.5, 7, 0.3); 
	 
	//glColor3d(100, 0, 0);
	//glColor3d(255, 255, 255);
	glColor3d(0, 0, 5);
	prisma(-14.6, -19.4, 0.3, 1.9, 0.2, 0.8);
	prisma(-14.6, -13.2, 0.3, 1.9, 0.2, 0.8);
	prisma(-12.9, -19.4, 0.3, 0.2, 6.2, 0.8);
	prisma(-14.6, -15.3, 0.3, 0.2, 2.1, 0.8);
	prisma(-14.6, -19.4, 0.3, 0.2, 2.1, 0.8);
	prisma(-14.6, -17.3, 0.9, 0.2, 2.0, 0.2);

	//glColor3d(155, 45, 63);////relleno 
	glColor3d(0, 0, 5);
	prisma(-21, -13, 0, 6, 1.5, 0.3);

	//glColor3d(100, 0, 0);
	//glColor3d(255, 255, 255);
	glColor3d(0, 0, 5);
	prisma(-20.6, -12.9, 0.3, 1.4, 0.2, 0.8);
	prisma(-20.6, -12.9, 0.3, 0.2, 1, 0.8);
	prisma(-20.6, -12.1, 0.3, 5.2, 0.2, 0.8);
	prisma(-15.6, -12.9, 0.3, 0.2, 1, 0.8);
	prisma(-17.0, -12.9, 0.3, 1.4, 0.2, 0.8);
	prisma(-19.2, -12.9, 0.9, 2.4, 0.2, 0.2);


	//glColor3d(155, 45, 63);
	glColor3d(0, 0, 5);
	prisma(-14.5, -11.5, 0, 9, 1, 0.3);
	prisma(-16, -10.8, 0, 1.5, 6.5, 0.3);
	prisma(-14.5, -3.3, 0, 9, 2.5, 0.6);
	prisma(-5.5, -10.8, 0, 1.5, 6.5, 0.3);
	prisma(-14.5, -4.3, 0, 3.5, 1, 0.6);
	prisma(-9, -4.3, 0, 3.5, 1, 0.6);
	prisma(-12.5, -0.8, 0, 5, 5, 0.6);

	glColor3d(0, 0, 5);
	//glColor3d(255, 255, 255);
	glBegin(GL_POLYGON);
	glVertex3f(-11, -4.3, 0);
	glVertex3f(-9, -4.3, 0);
	glVertex3f(-9, -3.3, 0.6);
	glVertex3f(-11, -3.3, 0.6);
	glEnd(); 
	
	
	//glColor3d(100, 0, 0);
	//glColor3d(255, 255, 255);
	glColor3d(0, 0, 5);
	prisma(-14.3, -11.3, 0.3, 8.6, 0.6, 0.4);
	prisma(-15.8, -10.6, 0.3, 1.1, 6.1, 0.3);
	prisma(-11, -3, 1.2, 2, 0.2, 0.2);
	prisma(-5.3, -10.6, 0.3, 1.1, 6.1, 0.3);
	
	//glColor3d(100, 0, 0);
	//glColor3d(255, 255, 255);
	glColor3d(0, 0, 5);
	prisma(-10.6, -3, 0.6, 0.4, 0.2, 0.6);
	prisma(-9.8, -3, 0.6, 0.4, 0.2, 0.6);
	prisma(-9.0, -3, 0.6, 3.2, 0.2, 0.8);
	prisma(-14.2, -3, 0.6, 3.2, 0.2, 0.8);
	prisma(-14.2, -3, 0.6, 0.2, 2, 0.8);
	prisma(-5.9, -3, 0.6, 0.2, 2, 0.8);
	prisma(-14.2, -1, 0.6, 4.8, 0.2, 0.8);
	prisma(-9, -1, 0.6, 3.3, 0.2, 0.8);
	prisma(-12.5, 2.6, 0.6, 4.8, 1.4, 0.8);
	prisma(-9.0, -0.8, 0.6, 1.4, 4.8, 0.8);
	prisma(-12.5, -0.8, 0.6, 1.4, 4.8, 0.8);
	prisma(-12.5, -0.8, 0.6, 3.1, 1.4, 0.8);
	prisma(-9.4, -0.8, 1.2, 0.4, 1.4, 0.2);

	//plaza central
	//glColor3d(100, 0, 0);
	//glColor3d(255, 255, 255);
	glColor3d(0, 0, 5);
	prisma(-21, 10, 0.0, 9, 6, 1.2);
	prisma(-21.3, 9.7, 0.0, 2, 2, 1.2);
	prisma(-21.3, 14.4, 0.0, 2, 2, 1.2);
	prisma(-17.5, 9.7, 0.0, 1.2, 6.6, 1.2);
	prisma(-14.0, 9.7, 0.0, 1.2, 6.6, 1.2);
	prisma(-12, 11, 0.0, 3, 4, 1);
	 
    //agrega relleno a un prisma
	//glColor3d(155, 45, 63);
	glColor3d(0, 0, 5);
	prisma(-16.5, 18.2, 0, 4.8, 1.0, 0.6); 

	//glColor3d(100, 0, 0);
	//glColor3d(255, 255, 255);
	glColor3d(0, 0, 5);
	prisma(-16.7, 18.1, 0, 1.4, 0.2, 0.8);
	prisma(-16.7, 18.1, 0, 0.2, 1.1, 0.8);
	prisma(-16.7, 19.2, 0, 5.2, 0.2, 0.8);
	prisma(-11.7, 18.1, 0, 0.2, 1.1, 0.8);
	prisma(-13.1, 18.1, 0, 1.4, 0.2, 0.8);
	prisma(-15.3, 18.1, 0.6, 2.4, 0.2, 0.2);
	
	//3 prismas que forman 3 lados de un cuadro
	//agrega relleno a un prisma
	//glColor3d(155, 45, 63);
	glColor3d(0, 0, 5);
	prisma(-11.2, 20.6, 0, 0.8, 6, 0.5); 
	
	//glColor3d(100, 0, 0);
	//glColor3d(255, 255, 255);
	glColor3d(0, 0, 5);
	prisma(-11.4, 20.4, 0, 1.2, 0.2, 0.8);
	prisma(-11.4, 26.6, 0, 1.2, 0.2, 0.8);
	prisma(-10.4, 20.4, 0, 0.2, 6.2, 0.8);
	prisma(-11.4, 24.5, 0, 0.2, 2.1, 0.8);
	prisma(-11.4, 20.4, 0, 0.2, 2.1, 0.8);
	prisma(-11.4, 22.5, 0.6, 0.2, 2.0, 0.2);
	
	//agrega relleno a un prisma
	//glColor3d(155, 45, 63);
	glColor3d(0, 0, 5);
	prisma(-16.5, 26.5, 0, 4.8, 1.1, 0.5); 

	//glColor3d(100, 0, 0);
	//glColor3d(255, 255, 255);
	glColor3d(0, 0, 5);
	prisma(-16.7, 26.4, 0, 1.4, 0.2, 0.8);
	prisma(-16.7, 26.4, 0, 0.2, 1.1, 0.8);
	prisma(-16.7, 27.5, 0, 4.0, 0.2, 0.8);
	prisma(-12.3, 27.5, 0, 0.8, 0.2, 0.8);
	prisma(-12.7, 27.5, 0.6, 0.4, 0.2, 0.2);
	prisma(-11.7, 26.4, 0, 0.2, 1.1, 0.8);
	prisma(-13.1, 26.4, 0, 1.4, 0.2, 0.8);
	prisma(-15.3, 26.4, 0.6, 2.4, 0.2, 0.2);

	//agrega relleno a un prisma
	//glColor3d(155, 45, 63);
	glColor3d(0, 0, 5);
	prisma(-17.7, 20.5, 0, 0.8, 6.2, 0.6); 

	//glColor3d(100, 0, 0);
	//glColor3d(255, 255, 255);
	glColor3d(0, 0, 5);
	prisma(-17.9, 20.4, 0, 1.2, 0.2, 0.8);
	prisma(-17.9, 26.6, 0, 1.2, 0.2, 0.8);
	prisma(-17.9, 20.4, 0, 0.2, 6.2, 0.8);
	prisma(-16.9, 24.5, 0, 0.2, 2.1, 0.8);
	prisma(-16.9, 20.4, 0, 0.2, 2.1, 0.8);
	prisma(-16.9, 22.5, 0.6, 0.2, 2.0, 0.2);

	camino();
	contador++;

	glPopMatrix();
	glutSwapBuffers();

}

void idle() {
	glutPostRedisplay();
}

int main(int argc, char** argv)
{
	llenarMatriz();
	//linea(-60*10+600,-40*10+400,5*10+600,40*10+400);
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	//glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
	glutInitWindowSize(1000, 800); 
	//glutInitWindowSize(3120,4160);
	glutInitWindowPosition(0, 0);
	glutCreateWindow("Recorrido");
//	initRendering();

	//init();
	glDepthFunc(GL_LEQUAL);
	glEnable(GL_DEPTH_TEST);
	glutReshapeFunc(reshape);
	glutDisplayFunc(display);
	glutSpecialFunc(Arrowkeys);
	glutMouseFunc(onMouse);
	//glutKeyboardFunc(keyboard);
	glutIdleFunc(idle);
	glutMainLoop();
	return 0;
}




